<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\subscriptionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home','HomeController@index');

Auth::routes();
Route::group(['middleware'=>['auth']],function() {
    Route::get('blogs','Blog\BlogController@index')->name('blog-list');
     Route::get('create-post','Blog\BlogController@createPost')->name('create-post');
    Route::post('upload','Blog\BlogController@upload')->name('upload');
    Route::post('storepost','Blog\BlogController@storePost')->name('storepost');
    Route::get('viewblog/{id}','Blog\BlogController@viewBlog')->name('view-blog');
    Route::get('edit/{id}','Blog\BlogController@editPost')->name('edit-post');
    Route::post('update/{id}','Blog\BlogController@updatePost')->name('update-post');
    Route::get('delete/{id}','Blog\BlogController@deletePost')->name('delete-post');
    Route::get('archieve/{id}','Blog\BlogController@archieve')->name('archieve-post');
    Route::get('archievelist','Blog\BlogController@archieveList')->name('archieve-post-list');
    Route::get('unarchieve/{id}','Blog\BlogController@unarchieve')->name('unarchieve');
    Route::get('profile','Blog\BlogController@profile')->name('profile');
    Route::post('profileupdate/{id}','Blog\BlogController@profileUpdate')->name('profile-update');
    Route::get('sortasc','Blog\BlogController@sortAsc')->name('sortasc');
    Route::get('draftlist','Blog\BlogController@draftBlog')->name('draft-blog');
    Route::get('draftpublish/{id}','Blog\BlogController@draftPublish')->name('draft-publish');
    Route::get('checkout','CheckoutController@checkout')->name('checkout.creditcard');
    Route::post('checkout','CheckoutController@afterpayment')->name('checkout.credit-card');
    Route::get('createtoken','CheckoutController@createtoken')->name('create-token');
    Route::get('savecard','CheckoutController@savecard')->name('save-card');
    Route::get('setdefaultcard','CheckoutController@setDefaultCard')->name('setdefault-card');
    Route::get('deletecard','CheckoutController@deleteCard')->name('delete-card');
    Route::get('createinvoice','CheckoutController@createInvoice')->name('create-invoice');
    // Route::get('createcharge','CheckoutController@createCharge')->name('create-charge');

    Route::get('paymentform','CheckoutController@paymentForm')->name('payment-form');


    // Recurring checkout session apis
    Route::get('/plans','subscriptionController@index')->name('plans-listing');
    Route::get('/checkoutform','subscriptionController@checkoutform')->name('checkout-form');
    Route::post('/submit','subscriptionController@submit')->name('submit-form');
});

Route::get('listcard','CheckoutController@listcards')->name('list-card');
