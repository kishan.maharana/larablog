<?php

namespace App;

use App\Plan;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function plans()
    {
        return $this->hasMany(Plan::class, 'product_id', 'id');
    }
}
