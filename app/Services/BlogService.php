<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\BlogRepository;
use Illuminate\Support\Facades\Log;

class BlogService
{
    /**
     * ActivityRepository instance
     * @var $blogRepository
     */
    protected $blogRepository;

    /**
     * ActivityService constructor.
     * @param BlogRepository $blogRepository
     */
    public function __construct(BlogRepository $blogRepository)
    {
        $this->blogRepository = $blogRepository;
    }
    /**
     * Blog list
     * @return [type] [description]
     */
    public function blogList()
    {
        try{
            $data = $this->blogRepository->blogList();
            return view('blogs.blogList',compact('data'));
        }
        catch (\Exception $e) {
            Log::error(
                'Failed to fetch data'
            );
            return false;
        }
    }
    /**
     * create a new post
     * @param  array $data getting form data
     * @return [type]       [description]
     */
    public function storePost($data)
    {
        try{
          $data = $this->blogRepository->storePost($data);
          if ($data) {
            Log::info('Data Stored Successfully', ['method' => __METHOD__]);
            return redirect(url('blogs'));
        } else {
            return response()->json(['response' => ['code' => 400, 'message' => 'Unable to add record']]);
        }
    }
    catch (\Exception $e) {
        Log::error(
            'Failed to fetch data'
        );
        return false;
    }
}
/**
 * 
 * View an perticular bog post
 * @param  int $id contains post id
 * @return [type]     [description]
 */
public function viewBlog($id)
{
    try{
      $data = $this->blogRepository->viewBlog($id);
      return $data;
  }
  catch (\Exception $e) {
    Log::error(
        'Internal server error'
    );
    return false;
}
}
/**
 * Edit and post
 * @param  int $id post id
 * @return [type]     [description]
 */
public function editPost($id)
{
    try{
      $data = $this->blogRepository->editPost($id);
      return $data;
  }
  catch (\Exception $e) {
    Log::error(
        'Internal server error'
    );
    return false;
}
}
/**
 * Update a post
 * @param  array $data contains the form data
 * @param  int $id   post id
 * @return [type]       [description]
 */
public function updatePost($data,$id)
{
    try{
       $data = $this->blogRepository->updatePost($data,$id);
       return $data;
   }
   catch (\Exception $e) {
    Log::error(
        'Unable to update record'
    );
    return false;
}
}
/**
 * Deleting a perticular post
 * @param  int $id contains post id
 * @return [type]     [description]
 */
public function deletePost($id)
{
    try{
      $data = $this->blogRepository->deletePost($id);
      return $data;
  }
  catch (\Exception $e) {
    Log::error(
        'Internal server error'
    );
    return false;
}
}
/**
 * archieving a post
 * @param  int $id contains post id
 * @return [type]     [description]
 */
public function archieve($id)
{
    try{
        $data = $this->blogRepository->archieve($id);
        return $data;
    }
    catch (\Exception $e) {
        Log::error(
            'Unable to update'
        );
        return false;
    }
}
/**
 * list of archieved posts
 * @return [type] [description]
 */
public function archieveList()
{
    try{
     $data = $this->blogRepository->archieveList();
     return $data;       
 }
 catch (\Exception $e) {
    Log::error(
        'Unable to fetch records'
    );
    return false;
}
}
/**
 * Unarchieving a post
 * @param  int $id contains post id
 * @return [type]     [description]
 */
public function unarchieve($id)
{
    try{
     $data = $this->blogRepository->unarchieve($id);
     return $data;       
 }
 catch (\Exception $e) {
    Log::error(
        'Unable to fetch records'
    );
    return false;
}
}
/**
 * Getting profile detail
 * @return [type] [description]
 */
public function profile()
{
    try{
     $data = $this->blogRepository->profile();
     return $data;       
 }
 catch (\Exception $e) {
    Log::error(
        'Unable to fetch records'
    );
    return false;
}
}
/**
 * Updating profile data
 * @param  array $data    contains form data
 * @param  int $id      contains user id
 * @param  array $request contains form data
 * @return [type]          [description]
 */
public function profileUpdate($data,$id,$request)
{
    try{
        
     $data = $this->blogRepository->profileUpdate($data,$id,$request);
     return $data;       
 }
 catch (\Exception $e) {
    Log::error(
        'Unable to fetch records'
    );
    return false;
}
}
/**
 * sorting the blog post in ascending order by created date
 * @return [type] [description]
 */
public function sortAsc()
{
    try{
     $data = $this->blogRepository->sortAsc();
     return $data;       
 }
 catch (\Exception $e) {
    Log::error(
        'Unable to fetch records'
    );
    return false;
}
}
/**
 * List of draft blogs
 * @return [type] [description]
 */
public function draftBlog()
{
    try{
       $data = $this->blogRepository->draftBlog();
       return $data;
   }
   catch (\Exception $e) {
    Log::error(
        'Unable to fetch records'
    );
    return false;
}
}
/**
 * Publishing the drafted post
 * @param  int $id contains post id
 * @return [type]     [description]
 */
public function draftPublish($id)
{
    try{
        $data = $this->blogRepository->draftPublish($id);
        return $data;
    }
    catch (\Exception $e) {
        Log::error(
            'Unable to fetch records'
        );
        return false;
    }
}

}
