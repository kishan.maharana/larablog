<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\BlogService;

class BlogController extends Controller
{
    protected $blogService;

    /**
     * ActivityController constructor.
     * @param BlogService $blogService
     */
    public function __construct(BlogService $blogService)
    {
        $this->blogService = $blogService;
    }
    /*
    Getting index page for blog listing
     */
    public function index()
    {
        return $this->blogService->blogList();
    }
    /**
     * Blog create form
     * @return [type]
     */
    public function createPost()
    {
        return view('blogs.createPost');
    }
    /**
     * storing blog post content
     * @param  array
     * @return [type]
     */
    public function storePost(Request $request)
    {
        $data = $request->all();
        return $this->blogService->storePost($data);
    }
    /**
     * view an perticular post
     * @param  int $id [description]
     * @return [type]     [description]
     */
    public function viewBlog($id)
    {
        return $this->blogService->viewBlog($id);
    }
    /**
     * Edit blog
     */
    public function editPost($id)
    {
        return $this->blogService->editPost($id);
    }
    /**
     * Update Post
     * @param  Request $request getting all data from form
     * @param  int  $id      post id
     * @return [type]           [description]
     */
    public function updatePost(Request $request,$id)
    {
        $data = $request->all();
        return $this->blogService->updatePost($data,$id);   
    }
    /**
     * Delete Post
     */
    public function deletePost($id)
    {
        return $this->blogService->deletePost($id);
    }
    /**
     * Archieving a post
     * @param  int $id post id
     * @return [type]     [description]
     */
    public function archieve($id)
    {
     return $this->blogService->archieve($id);   
 }
    /**
     * List of archieved posts
     * @return [type] [description]
     */
    public function archieveList()
    {
     return $this->blogService->archieveList();      
 }
    /**
     * Unarchieve a post
     * @param  int $id post id
     * @return [type]     [description]
     */
    public function unarchieve($id)
    {
     return $this->blogService->unarchieve($id);   
 }
    /**
     * Getting profile detail of logged in user
     * @return [type] [description]
     */
    public function profile()
    {
     return $this->blogService->profile();   
 }
    /**
     * Updating profile detail with profile image
     * @param  Request $request getting form data
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function profileUpdate(Request $request,$id)
    {

     $data = $request->all();
     return $this->blogService->profileUpdate($data,$id,$request);   
 }
    /**
     * Soting the blog post in ascending order by date
     * @return [type] [description]
     */
    public function sortAsc()
    {
        return $this->blogService->sortAsc();   
    }
    /**
     * List of Draft blogs
     * @return [type] [description]
     */
    public function draftBlog()
    {
        return $this->blogService->draftBlog(); 
    }
    public function draftPublish($id)
    {
       return $this->blogService->draftPublish($id);    
   }
/**
 * code for uploading image ckeditor
 * @param  Request
 * @return [type]
 */
public function upload(Request $request)
{
    if($request->hasFile('upload')) {
            //get filename with extension
        $filenamewithextension = $request->file('upload')->getClientOriginalName();

            //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
        $extension = $request->file('upload')->getClientOriginalExtension();

            //filename to store
        $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
        $request->file('upload')->storeAs('public/uploads', $filenametostore);

        $CKEditorFuncNum = $request->input('CKEditorFuncNum');
        $url = asset('storage/uploads/'.$filenametostore);
        $msg = 'Image successfully uploaded';
        $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            // Render HTML output
        @header('Content-type: text/html; charset=utf-8');
        echo $re;
    }
}

}
