<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Product;
use Carbon\Carbon;
use App\customer_id_user;
use App\SubscriptionItem;
use App\SubscriptionDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class subscriptionController extends Controller
{
    public function index()
    {
        $product = Product::with('plans')->get();
        return view('subscription.plan',compact('product'));
    }
    public function checkoutform()
    {
        return view('subscription.checkout');
    }
    public function submit(Request $request)
    {
        try{
        $data = $request->all();
        $plan_id = 'price_1JDjemSCwsVvt8rmSP8vdVHV';
        $product_id = 'prod_JrSTlsd3gDzKyY';
        $token = $data['stripeToken'];
        $stripeEmail = $data['stripeEmail'];

        // check if customer is exist on behalf of the email

        $check_customer = customer_id_user::where('email',$stripeEmail)->first();
        if($check_customer)
        {
            // dd($check_customer);
            $customer_id = $check_customer->customer_id;
            if($token)
            {
                // create subscription
                $stripe = new \Stripe\StripeClient(
                    'sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR'
                  );
                $subscription = $stripe->subscriptions->create([
                    'customer' => $customer_id,
                    'items' => [
                      ['price' => $plan_id],
                    ],
                ]);
                // save subscription data to local DB.
                if($subscription)
                {
                    $subscription = $this->saveSubscriptionToLocal($subscription);
                }
                return $subscription;
            }
            else{
                return 'Token not found';
            }
        }
        else{
            // dd('inside else');

            // create a payment method before creating customer

            $stripe = new \Stripe\StripeClient(
                'sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR'
              );
            $payment_method = $stripe->paymentMethods->create([
                'type' => 'card',
                'card' => [
                  'number' => '4242424242424242',
                  'exp_month' => 7,
                  'exp_year' => 2022,
                  'cvc' => '314',
                ],
              ]);
            //   return $payment_method;
            // create a new customer on stripe as well as local DB
            \Stripe\Stripe::setApiKey('sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR');
            $create = \Stripe\Customer::create([
                'email' => $stripeEmail,
                'description' => 'My First Test Customer (created for API docs)',
                "address" => ["city" => 'phulbani', "country" => 'india', "line1" => 'address', "line2" => "", "postal_code" => '762001', "state" => 'odisha'],
                'payment_method' => $payment_method['id'],
                'invoice_settings' => [
                'default_payment_method' => $payment_method['id'],
                ],
            ]);
            $customer_id = $create['id'];
            $obj = new customer_id_user;
            $obj->user_id = Auth::user()->id;
            $obj->email = $stripeEmail;
            $obj->customer_id = $customer_id;
            $obj->save();

            // create subscription
            $stripe = new \Stripe\StripeClient(
                'sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR'
              );
            $subscription = $stripe->subscriptions->create([
                'customer' => $customer_id,
                'items' => [
                  ['price' => $plan_id],
                ],
            ]);
            if($subscription)
            {
                $subscription = $this->saveSubscriptionToLocal($subscription);
            }
            return $subscription;
            
        }
    }
    catch(\Exception $e)
    {
        dd($e);
    }
    }
    public function saveSubscriptionToLocal($subscription)
    {
                    $sub_obj = new SubscriptionDetail;
                    $sub_obj->subscription_id = $subscription['id'];
                    $sub_obj->collection_method = $subscription['collection_method'];
                    $sub_obj->period_start = $subscription['current_period_start']??"";
                    $sub_obj->period_end = $subscription['current_period_end']??"";
                    $sub_obj->customer_id = $subscription['customer'];
                    $sub_obj->quantity = $subscription['quantity'];
                    $sub_obj->discount = $subscription['discount'];
                    $sub_obj->schedules = $subscription['schedule'];
                    $sub_obj->status = $subscription['status'];
                    $sub_obj->recurring_status = true;
                    $sub_obj->email = Auth::user()->email;
                    $sub_obj->user_id = Auth::user()->id;
                    $sub_obj->date = Carbon::now()->toDateTimeString();
                    $sub_obj->save();
                    if(isset($subscription['items']['data']))
                    {
                        foreach($subscription['items']['data'] as $key=> $value)
                        {
                            $item = new SubscriptionItem;
                            $item->sub_item_id = $value['id'];
                            $item->subscription_id = $sub_obj->id;
                            $item->created = $value['created'];
                            $item->plan_id = $value['plan']['id'];
                            $item->amount = $value['plan']['amount'];
                            $item->currency = $value['plan']['currency'];
                            $item->interval = $value['plan']['interval'];
                            $item->interval_count = $value['plan']['interval_count'];
                            $item->product_id = $value['plan']['product'];
                            $item->save();
                        }
                    }
                    $invoice = new Invoice;
                    $invoice->invoice_id = $subscription['latest_invoice'];
                    $invoice->subscription_id = $sub_obj->id;
                    $invoice->email = Auth::user()->email;
                    $invoice->user_id = Auth::user()->id;
                    $invoice->date = Carbon::now()->toDateTimeString();
                    $invoice->save();
                    return true;
    }
}
