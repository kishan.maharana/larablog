<?php

namespace App\Http\Controllers;
use Auth;
use Carbon\Carbon;
use Stripe\Stripe;
use App\CardDetail;
use App\customer_id_user;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function checkout()
    {   
        // dd('inside');
        // Enter Your Stripe Secret
        \Stripe\Stripe::setApiKey('sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR');
        		
		$amount = 100;
		$amount *= 100;
        $amount = (int) $amount;
        
        $payment_intent = \Stripe\PaymentIntent::create([
			'description' => 'Stripe Test Payment',
			'amount' => $amount,
			'currency' => 'INR',
			'description' => 'Payment From workstatus',
			'payment_method_types' => ['card'],
		]);
		$intent = $payment_intent->client_secret;

		return view('checkout.creditcard',compact('intent'));

    }

    public function afterPayment()
    {
        echo 'Payment Has been Received';
    }
    public function createtoken()
    {
        return view('checkout.createtoken');
    }
    public function savecard()
    {
        

        // test code with paymentid
        \Stripe\Stripe::setApiKey('sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR');
        $payment_id = 'pm_1JDn20SCwsVvt8rmoEiRcl1r';
        // This creates a new Customer and attaches the PaymentMethod in one API call.
        // check customer id behalf of email in DB
        $get_cus = customer_id_user::where('email',Auth::user()->email)->first();
        if($get_cus && $get_cus != NULL)
        {
          $customer_id = $get_cus->customer_id;
          $get_card_count = CardDetail::where('customer_id',$customer_id)->count();
          if($get_card_count > 0)
          {
            
            $default = false;
          }
          else{
            $default = true;
          }
        }
        else{
          $create = \Stripe\Customer::create([
              'email' => Auth::user()->email,
              'description' => 'My First Test Customer (created for API docs)',
          ]);
          $customer_id = $create['id'];
          $obj = new customer_id_user;
          $obj->user_id = Auth::user()->id;
          $obj->email = Auth::user()->email;
          $obj->customer_id = $customer_id;
          $obj->save();
          $default = true;
        }
// return $create;
        $payment_method = \Stripe\PaymentMethod::retrieve($payment_id);
        // return $create;
        // test code

            $stripe = new \Stripe\StripeClient(
                'sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR'
              );
             $tid =  $stripe->tokens->create([
                'card' => [
                  'number' => '4242424242424242',
                  'exp_month' => 7,
                  'exp_year' => 2022,
                  'cvc' => '314',
                ],
              ]);
              // following code creates a card for this customer which takes customer id and tok_mastercard as source
              $final = $stripe->customers->createSource(
                $customer_id,
                ['source' => $tid['id']] 
              );
        // dd($final,$payment_method);
        //end of test code
        $card = new CardDetail;
        $card->payment_id = $payment_method['id'];
        $card->customer_id = $customer_id;
        $card->name = $payment_method['billing_details']['name'];
        $card->cvc_check = $payment_method['card']['checks']['cvc_check'];
        $card->exp_month = $payment_method['card']['exp_month'];
        $card->exp_year = $payment_method['card']['exp_year'];
        $card->fingerprint = $payment_method['card']['fingerprint'];
        $card->funding = $payment_method['card']['funding'];
        $card->created = $payment_method['created'];
        $card->type = $payment_method['type'];
        $card->brand = $payment_method['card']['brand'];
        $card->country = $payment_method['card']['country'];
        $card->email = Auth::user()->email;
        $card->card_number = $payment_method['card']['last4'];
        $card->org_id = 28;
        $card->user_id = Auth::user()->id;
        $card->date = Carbon::now()->toDateTimeString();
        $card->card_id = $final['id'] ?? NULL;
        $card->default_card = $default;
        // return 
        return $card->save();
        // return $final;
        return $payment_method;

        // NOTE

        // WE ALSO NEED TO SET DEFAULT CARD OPTION IN DB SO THAT WE CAN CHECK WHICH CARD IS GOING TO BE USED IN THE FUTURE PAYMENT
        
    }
    public function listcards()
    {
      try{
        \Stripe\Stripe::setApiKey('sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR');
        // $test = \Stripe\Customer::retrieve("cus_JqMqk1NSRF5cBy");
        $get_cus = customer_id_user::where('email','admin@yopmail.com')->first();
        // return $get_cus->customer_id;
        $data = [];
        if($get_cus && $get_cus != NULL)
        {
          $stripe = new \Stripe\StripeClient(
            'sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR'
          );
          $data = $stripe->customers->allSources(
            $get_cus->customer_id,
            ['object' => 'card', 'limit' => 5]
          );
          // return $data;
        }
        else
        {
          $data = [];
        }
        if($data)
        {
          $list = [];
          foreach($data as $key => $value)
          {
            $list[$key]['id'] = $value['id'];
            $list[$key]['customer'] = $value['customer'];
            $list[$key]['last4'] = $value['last4'];
            $list[$key]['exp_month'] = $value['exp_month'];
            $list[$key]['exp_year'] = $value['exp_year'];
            $local_data = CardDetail::where('customer_id',$value['customer'])
                                    ->where('card_id',$value['id'])->first();
            $list[$key]['default'] = $local_data->default_card ?? 0;
            $list[$key]['name'] = $local_data->name;
          }
        }
        else{
          $list = [];
        }
        return view('checkout.cardslisting',compact('list'));
      }
      catch(\Exception $e)
      {
        dd($e);
      }
        
        // NOTE
        
        // NEED TO FETCH DATA FROM LOCAL TABLE ALSO TO PROVIDE ORG ID AND USER ID AND EMAIL ON LISTING 
    }
    public function setDefaultCard()
    {
      Stripe::setApiKey('sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR');
      // customer_id and card_id will come from frontend after clicking on radio button
      $stripe_cus_id = 'cus_Jr6YVFfPp8EBKM';
      $card_id = 'card_1JDOMnSCwsVvt8rmET1X1r70';
      $customer = \Stripe\Customer::retrieve($stripe_cus_id);
      $customer->default_source= $card_id;
      $change_default = $customer->save();
      if($change_default)
      {
        $update = CardDetail::where('customer_id',$stripe_cus_id)->update(['default_card'=>0]);
        $update_default = CardDetail::where('customer_id',$stripe_cus_id)
                                    ->where('card_id',$card_id)
                                    ->update(['default_card'=>1]);
        return $update_default;
      }
      else{
        return false;
      }
    }
    public function deleteCard()
    {
        // get customer id and card_id from frontend
        $cus_id = 'cus_Jr6YVFfPp8EBKM';
        $card_id = 'card_1JDOLhSCwsVvt8rmJKzOHM3j';
        $stripe = new \Stripe\StripeClient(
          'sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR'
        );
        $delete_card = $stripe->customers->deleteSource(
          $cus_id,
          $card_id,
          []
        );
        // delete card from local DB
        $delete_local = CardDetail::where('customer_id',$cus_id)
                                  ->where('card_id',$card_id)
                                  ->first();
        $delete_id = $delete_local->id;
                                  // dd($delete_local);
        
          $desc = CardDetail::where('customer_id',$cus_id)
                            ->where('id','>',$delete_id)
                            ->orderBy('id','desc')                      
                            ->first();
                            // dd($desc);
                            // $desc = NULL;
          if($desc && $desc!=NULL)
          {
            $delete_local->delete();
            $desc->default_card = 1;
            return $desc->save();
          }
          else{
            $asc = CardDetail::where('customer_id',$cus_id)
                            ->where('id','<',$delete_id)
                            ->orderBy('id','DESC')                      
                            ->first();
            if($asc && $asc!=NULL)
            {
              $delete_local->delete();
              $asc->default_card = 1;
              return $asc->save();
            }
            else{
              // $delete_customer_id = customer_id_user::where('customer_id',$cus_id)->delete();
              return $delete_local->delete();
            }
          }
       

        return $delete_card;
    }
    public function createInvoice()
    {
        $stripe = new \Stripe\StripeClient(
          'sk_test_51JCN97SCwsVvt8rmDcJwnGjgHTn6cpKI9CwtKiF3TAGjjKGywsXiOry5xtqXJdJ8KN2cML0u8iqH5R9ebPUnd3xi00sUPSLHrR'
        );
        $invoice = $stripe->invoices->create([
          'customer' => 'cus_JqljvVSUW7hZP1',
        ]);
        return $invoice;

    }
    public function paymentForm()
    {
      return view('checkout.paymentform');
    }
}
