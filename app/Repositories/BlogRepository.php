<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Post;
use App\Image;
use Carbon\Carbon;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Log;

class BlogRepository
{
    /**
     * Getting blog list
     * @return [type] [description]
     */
    public function blogList()
    {
        try{
            $user_id = Auth::user()->id;
            $data = Post::where('user_id',$user_id)
            ->where('status',1)
            ->where('is_archieve',0)
            ->where('is_draft',0)
            ->orderBy('id', 'DESC')->paginate(5);
            return $data;
        }
        catch (\Exception $e) {
            dd($e);
            Log::error(
                'Failed to fetch data'
            );
            return false;
        }
    }
    /**
     * Creating new blog post
     * @param  array $data contains form data
     * @return [type]       [description]
     */
    public function storePost($data)
    {
    	try{
            // dd($data['draft']);
            if(array_key_exists('publish',$data))
            {
                $post = new Post;
                $post->user_id = Auth::user()->id;
                $post->title = $data['blogtitle'];
                $post->body = $data['editor'];
                $post->date = Carbon::now()->toDateString();
                $post->is_draft = 0;
                $save = $post->save();
                return $save;
            }
            else
            {
                $post = new Post;
                $post->user_id = Auth::user()->id;
                $post->title = $data['blogtitle'];
                $post->body = $data['editor'];
                $post->date = Carbon::now()->toDateString();
                $post->is_draft = 1;
                $save = $post->save();
                return $save;
            }

        }
        catch (\Exception $e) {
            Log::error(
                'Failed to fetch data'
            );
            return false;
        }
    }
    /**
     * view a perticular post
     * @param  int $id contains post id
     * @return [type]     [description]
     */
    public function viewBlog($id)
    {
        try{
            $data = Post::where('id',$id)->first();
            $views=$data->views;
            $views++;
            $data->views = $views;
            $data->save();

            return view('blogs.bogView',compact('data'));
        }
        catch (\Exception $e) {
            Log::error(
                'Internal server error'
            );
            return false;
        }
    }
    /**
     * edit a post
     * @param  int $id contains post id
     * @return [type]     [description]
     */
    public function editPost($id)
    {
     try{
        $data = Post::find($id);
        return view('blogs.updatePost',compact('data'));
    }
    catch (\Exception $e) {
        Log::error(
            'Internal server error'
        );
        return false;
    }
}
    /**
     * Updating a blog post
     * @param  array $data contains form data
     * @param  int $id   contains post id
     * @return [type]       [description]
     */
    public function updatePost($data,$id)
    {
     try{
        $post = Post::find($id);
        $post->title = $data['blogtitle'];
        $post->body = $data['editor'];
        $save = $post->save();
        return redirect(url('/blogs'));
    }
    catch (\Exception $e) {
        Log::error(
            'Internal server error'
        );
        return false;
    }
}
    /**
     * delete a post
     * @param  int $id contains post id
     * @return [type]     [description]
     */
    public function deletePost($id)
    {
     try{
        $delete = Post::find($id)->delete();
        return redirect(url('/blogs'));
    }
    catch (\Exception $e) {
        Log::error(
            'Internal server error'
        );
        return false;
    }
}
    /**
     * archieve a post
     * @param  int $id contains post id
     * @return [type]     [description]
     */
    public function archieve($id)
    {
     try{
        $data = Post::find($id);
        if($data->is_archieve == 0)
        {
            $data->is_archieve = 1;
        }
        else
        {
            $data->is_archieve = 0;
        }
        $data->save();
        return redirect(url('/blogs'));
    }
    catch (\Exception $e) {
        Log::error(
            'unabe to update'
        );
        return false;
    }
}
    /**
     * getting archieve post list
     * @return [type] [description]
     */
    public function archieveList()
    {
     try{
        $data = Post::where('is_archieve',1)->get();
        return view('blogs.archieveList',compact('data'));
    }
    catch (\Exception $e) {
        Log::error(
            'unabe to fetch records'
        );
        return false;
    }
}
    /**
     * Unarchieving a post
     * @param  int $id contains post id
     * @return [type]     [description]
     */
    public function unarchieve($id)
    {
     try{
        $data = Post::find($id);
        $data->is_archieve = 0;
        $save = $data->save();
        return redirect(url('/archievelist'));
    }
    catch (\Exception $e) {
        Log::error(
            'unabe to update'
        );
        return false;
    }
}
    /**
     * Getting profile detail
     * @return [type] [description]
     */
    public function profile()
    {
     try{
        $user_id = Auth::user()->id;
        $profile = User::select('*')
        ->with(
            [
                'image' => function ($q) {
                    $q->select('id','user_id', 'image','phone');
                },
            ]
        )
        ->where('id', $user_id)->first();

        return view('blogs.profile',compact('profile'));
    }
    catch (\Exception $e) {
        dd($e);
        Log::error(
            'unabe to update'
        );
        return false;
    }
}
    /**
     * Updating profile detail
     * @param  array $data    contains form data
     * @param  int $id      contains user id
     * @param  array $request contains form data
     * @return [type]          [description]
     */
    public function profileUpdate($data,$id,$request)
    {
     try{

        $user = User::find($id);
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user_save = $user->save();
        $image = Image::where('user_id',$id)->first();
        
        if(empty($image))
        {
            $image = new Image;
            $image->user_id = $id;
            $image->phone = $data['phone'];

            if($request->hasfile('image'))  
            {  
                $file=$request->file('image');  
                $extension=$file->getClientOriginalExtension();  
                $filename=time().'.'.$extension;  
                $file->move(public_path()."/userimg/",$filename);
                $image->image=$filename;  
            }  
            else  
            {  
                $image->image='';  
            }  
            $image = $image->save();
            return redirect(url('/profile'));

        }
        else
        {
         if($request->hasfile('image'))  
         {  
            // dd('hello');
            $file=$request->file('image');  
            $extension=$file->getClientOriginalExtension();  
            $filename=time().'.'.$extension;  
            $file->move(public_path()."/userimg/",$filename);
            $image->image=$filename;  
        }  
        
        $image->phone = $data['phone'];
        $image_save = $image->save();
        return redirect(url('/profile'));
    }

}
catch (\Exception $e) {
    dd($e);
    Log::error(
        'unabe to update'
    );
    return false;
}
}
/**
 * sorting blog post by created at data ascending order
 * @return [type] [description]
 */
public function sortAsc()
{
    try{
        $user_id = Auth::user()->id;
        $data = Post::where('user_id',$user_id)
        ->where('status',1)
        ->where('is_archieve',0)
        ->where('is_draft',0)
        ->orderBy('id', 'ASC')->paginate(10);
        return view('blogs.blogList',compact('data'));
    }
    catch (\Exception $e) {
            // dd($e);
        Log::error(
            'Failed to fetch data'
        );
        return false;
    }
}
/**
 * Saving a blog as draft
 * @return [type] [description]
 */
public function draftBlog()
{
    try{
        $data = Post::where('is_draft',1)->get();
        return view('blogs.draftList',compact('data'));
    }
    catch (\Exception $e) {
            // dd($e);
        Log::error(
            'Failed to fetch data'
        );
        return false;
    }
}
/**
 * publishing the draft post
 * @param  int $id contains post id
 * @return [type]     [description]
 */
public function draftPublish($id)
{
    try{
        $post = Post::find($id);
        $post->is_draft = 0;
        $post->save();
        return redirect(url('/draftlist'));
    }
    catch (\Exception $e) {
            // dd($e);
        Log::error(
            'Failed to fetch data'
        );
        return false;
    }
}
}
