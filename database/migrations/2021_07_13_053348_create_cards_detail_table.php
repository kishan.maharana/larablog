<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards_detail', function (Blueprint $table) {
            $table->id();
            $table->string('payment_id');
            $table->string('customer_id');
            $table->string('name');
            $table->string('cvc_check')->nullable();
            $table->string('exp_month');
            $table->string('exp_year');
            $table->string('fingerprint')->nullable();
            $table->string('funding')->nullable();
            $table->string('created');
            $table->string('type')->nullable();
            $table->string('brand')->nullable();
            $table->string('country')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('email')->nullable();
            $table->Integer('card_number');
            $table->Integer('org_id');
            $table->Integer('user_id');
            $table->timestamp('date');
            $table->string('card_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards_detail');
    }
}
