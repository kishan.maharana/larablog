<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_item', function (Blueprint $table) {
            $table->id();
            $table->string('sub_item_id');
            $table->integer('subscription_id');
            $table->string('created');
            $table->string('plan_id');
            $table->bigInteger('amount');
            $table->string('currency');
            $table->string('interval')->nullable();
            $table->string('interval_count');
            $table->string('product_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_item');
    }
}
