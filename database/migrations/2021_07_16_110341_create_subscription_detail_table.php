<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_detail', function (Blueprint $table) {
            $table->id();
            $table->string('subscription_id');
            $table->string('collection_method')->nullable();
            $table->string('period_start');
            $table->string('period_end');
            $table->string('customer_id');
            $table->bigInteger('quantity');
            $table->string('discount')->nullable();
            $table->string('schedules')->nullable();
            $table->string('status')->nullable();
            $table->boolean('recurring_status')->default(0);
            $table->string('email')->nullable();
            $table->integer('user_id')->nullable();
            $table->dateTime('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_detail');
    }
}
