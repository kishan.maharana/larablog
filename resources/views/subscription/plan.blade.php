@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="card-group">
            @foreach($product as $key => $value)
            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">{{$value['name']}}</h5>
                  <p class="card-text">{{$value['description']}}</p>
                  @foreach($value['plans'] as $lkey => $val)
                    <button class="btn btn-primary" value="{{$val['price_id']}}">{{$val['price']}} / {{$val['duration']}}<button>
                  @endforeach
                </div>
              </div>
            
            @endforeach
        </div>
    </div>
</div>
@endsection