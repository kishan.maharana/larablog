@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <form action="{{route('submit-form')}}" method="POST">
            @csrf
            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_51JCN97SCwsVvt8rmXWwwjXJDduBSTB6PV2ThRgH2BpJIUqIZnsZSdq5X6ULUmVJqk2sxcM2ZkD5UTCmGPClumm5j00nk0TxNIW",
                data-amount="500"
                data-name="Gold Plan"
                data-description="dummy description",
                data-image="",
                data-currency="usd"
            >
            </script>
        </form>
    </div>
</div>
@endsection