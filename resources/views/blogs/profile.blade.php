@extends('layout.master');

@section('title')
Blogs
@endsection
@section('content')
<div class="row my-5">
	<div class="col-md-12 text-center">
		Profile
	</div>
</div>
<div class="row">
	<div class="col-md-12 text-center">
		{{-- {{dd($profile->image->image)}} --}}
		<?php

			if($profile->image != null)
			{
				$image = $profile->image->image;
				$phone = $profile->image->phone;
			}
			else
			{
				$image = '';
				$phone = '';
			}
		?>

		<img src="{{asset('/userimg').'/'.$image}}" style="height: 100px;width: 100px;border-radius: 50%;">
	</div>
</div>
<div class="row">
	<div class="col-md-12 text-center">
		{{-- {{dd($profile)}} --}}
		
	<form action="{{url('/profileupdate/'.$profile->id)}}" method="POST" enctype="multipart/form-data">
		@csrf

	<input type="text" name="name" class="form-control" value="{{$profile->name}}">
	<input type="text" name="email" class="form-control" value="{{$profile->email}}">
	<input type="text" name="phone" class="form-control" value="{{$phone}}">
	<p class="text-left">Choose profile picture</p>
	<input type="file" class="form-control-file" name="image">
	<input type="submit" class="btn btn-primary" name="" value="update">
</form>
	</div>
</div>
@endsection