@extends('layout.master');

@section('title')
Blogs
@endsection
@section('content')
<div class="row">

 <div class="col-md-12 text-center">
    <a href="{{url('/create-post')}}"><button class="btn btn-danger">Create new blog</button></a>
</div>

</div>



<div class="row my-3">
  <div class="col-md-12 text-center">
      <h2>Your Blogs</h2>    
  </div>
</div>

<div class="row my-3">
  <div class="col-md-8 text-center">
      
  </div>
  <div class="col-md-2 text-center">
      <h5>Sort-By</h5>
  </div>
  <div class="col-md-1 text-center">
     <a href="{{url('/sortasc')}}" type="button" class="btn btn-sm btn-outline-primary">First created</a>
  </div>
  <div class="col-md-1 text-center">
      <a href="{{url('/blogs')}}" type="button" class="btn btn-sm btn-outline-primary">Last Created</a>
  </div>
</div>


@foreach($data as $val)
<div class="row">
   <div class="col-md-12 text-center">
      <a href="{{url('/viewblog/'.$val->id)}}" class="list-group-item list-group-item-action active my-2">
        <div class="d-flex w-100 justify-content-between">
          <h5 class="mb-1">{{$val->title}}</h5>

          <small>{{$val->date}} &nbsp&nbsp&nbsp Total views : {{$val->views}}</small>
      </div>
      <small>Last updated on : {{$val->updated_at}}</small>

  </a>
</div>
</div>
<div class="row">
    <div class="col-md-1">
        <a href="{{url('/edit/'.$val->id)}}"><button class="btn btn-danger">Edit</button></a>
    </div>
    <div class="col-md-1">
        <a href="{{url('/archieve/'.$val->id)}}"><button class="btn btn-danger">Archieve</button></a>
    </div>
    <div class="col-md-1">
        <a href="{{url('/delete/'.$val->id)}}"><button class="btn btn-danger">Delete</button></a>
    </div>
    <div class="col-md-9">

    </div>
</div>
@endforeach

{{$data->links()}}

{{-- <p>{{$data}}</p> --}}

@endsection

