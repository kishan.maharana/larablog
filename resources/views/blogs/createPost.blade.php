@extends('layout.master');

@section('title')
    Create Post
@endsection
@section('content')
<div class="row mt-5">
    <div class="col-md-12">
        <form action="{{url('/storepost')}}" id="submitform" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="text" name="blogtitle" placeholder="Enter the blog Title" class="form-control" >
            <textarea class="form-control" id="editor" name="editor" rows="3"></textarea>
            <input class="btn btn-success" type="submit" value="publish" name="publish" />
    <input class="btn btn-success" type="submit" value="Save as Draft" name="draft" />
        </form>
    </div>

</div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editor',{
            filebrowserUploadUrl:"{{route('upload',['_token' => csrf_token()])}}",
            filebrowserUploadMethod:'form'
        });
    </script>
@endsection
