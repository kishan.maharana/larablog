@extends('layout.master');

@section('title')
Blogs
@endsection
@section('content')

<div class="row my-5">
  <div class="col-md-12 text-center">
    <h2>Archieved Blogs</h2>    
  </div>
</div>


@foreach($data as $val)
<div class="row">
 <div class="col-md-12 text-center">
  <a href="{{url('/viewblog/'.$val->id)}}" class="list-group-item list-group-item-action active my-2">
    <div class="d-flex w-100 justify-content-between">
      <h5 class="mb-1">{{$val->title}}</h5>

      <small>{{$val->date}}</small>
    </div>
    <small>Last updated on : {{$val->updated_at}}</small>

  </a>
</div>
</div>
<div class="row">
  <div class="col-md-1">
    <a href="{{url('/unarchieve/'.$val->id)}}"><button class="btn btn-danger">Unarchieve</button></a>
  </div>
  <div class="col-md-11">

  </div>
</div>
@endforeach

@endsection

