<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
  </head>
  <body>
    {{-- {{dd($list)}} --}}
    <div class="row my-5">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Card ID</th>
                    <th scope="col">Customer ID</th>
                    <th scope="col">Card Number</th>
                    <th scope="col">EXP-MONTH</th>
                    <th scope="col">EXP-YEAR</th>
                    <th scope="col">Default Card</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($list as $key => $value)
                  <tr>
                    <th scope="row">{{$value['name']}}</th>
                    <td>{{$value['id']}}</td>
                    <td>{{$value['customer']}}</td>
                    <td>********{{$value['last4']}}</td>
                    <td>{{$value['exp_month']}}</td>
                    <td>{{$value['exp_year']}}</td>
                    <td>{{$value['default']}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>
        <div class="col-md-2"></div>
    </div>
  </body>
</html>